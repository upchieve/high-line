require("newrelic");
const express = require("express");
const fs = require("fs");
const Mustache = require("mustache");
const logger = require("pino")();
const path = require("path");
const cacheControl = require("express-cache-controller");

const version = process.env.HIGH_LINE_VERSION;

const app = express();

const indexHtml = renderIndexHtml();

app.use(
  cacheControl({
    noCache: true
  })
);

app.get("/healthz", function(req, res, next) {
  res
    .json({
      version
    })
    .status(200);
  next();
});

app.use(express.static(path.join(__dirname, "dist")));

app.use((req, res, next) => {
  if (req.path.includes("healthz")) {
    next();
    return;
  }
  res.send(indexHtml).status(200);
  next();
});

app.listen(8080, () => {
  logger.info("listening on 8080");
});

function renderIndexHtml() {
  let template = "";
  const indexPath = path.join(__dirname, "index.html");
  try {
    template = fs.readFileSync(indexPath, "utf8");
  } catch (err) {
    logger.error(`error reading index.html file: ${err}`);
    process.exit(1);
  }
  // speeds up rendering on the first request
  Mustache.parse(template);

  const config = {
    zwibblerUrl: process.env.VUE_APP_ZWIBBLER_URL,
    websocketRoot: process.env.VUE_APP_WEBSOCKET_ROOT,
    serverRoot: process.env.VUE_APP_SERVER_ROOT,
    socketAddress: process.env.VUE_APP_SOCKET_ADDRESS,
    mainWebsiteUrl: process.env.VUE_APP_MAIN_WEBSITE_URL,
    posthogToken: process.env.VUE_APP_POSTHOG_TOKEN,
    papercupsId: process.env.VUE_APP_PAPERCUPS_ID,
    unleashUrl: process.env.VUE_APP_UNLEASH_URL,
    unleashName: process.env.VUE_APP_UNLEASH_NAME,
    unleashId: process.env.VUE_APP_UNLEASH_ID,
    newRelicBrowserAccountId: process.env.VUE_APP_NEW_RELIC_ACCOUNT_ID,
    newRelicBrowserTrustKey: process.env.VUE_APP_NEW_RELIC_TRUST_KEY,
    newRelicBrowserAgentId: process.env.VUE_APP_NEW_RELIC_AGENT_ID,
    newRelicBrowserLicenseKey: process.env.VUE_APP_NEW_RELIC_LICENSE_KEY,
    newRelicBrowserAppId: process.env.VUE_APP_NEW_RELIC_APP_ID
  };

  return Mustache.render(template, config);
}
